<?php

class Data{

	public function setBoxes($json, $img){

		$data = getimagesize($img);
		$w = $data[0];
		$h = $data[1];
		$dataObj = [];


		foreach($json->objects as $key3=>$object){

			$dataObj[$key3] = new stdClass;
			$dataObj[$key3]->x = round(100 * $object->bbox[0] / $w, 3);
			$dataObj[$key3]->y = round(100 * $object->bbox[1] / $h, 3);
			$dataObj[$key3]->label = $object->label;

		}

		return $dataObj;

	}

	public function getWhiteImgs(){

		$allImages = glob("../cutImg/*");
		$images = [];

		shuffle($allImages);

		foreach($allImages as $key=>$image){

			$fileinfo = pathinfo($image);
			$json = json_decode(file_get_contents("../json/".$fileinfo["filename"].".json"));
			
			$images[$key] = new stdClass;
			$images[$key]->path = $image;

			if(count($json->objects) > 0){

				$images[$key]->objects = $this->setBoxes($json, $image);

			}

		}

		return $images;
	}

	public function getLibrary(){

		$library = glob("../library/*", GLOB_ONLYDIR);
		$data = [];

		foreach($library as $key=>$path){

			$data[$key] = new stdClass;

			$data[$key]->id = str_replace("_", " ", basename($path));
			$data[$key]->name = basename($path);
			$data[$key]->images = [];

			$images = glob($path."/*");

			foreach($images as $image){

				if($images !== ".DS_Store"){

					$data[$key]->images[] = $image;
				}
			}

		}

		return $data;
	}
}