from PIL import Image, ImageFont, ImageDraw, ImageEnhance
import glob, os, subprocess, json, random
from pathlib import Path
import urllib2
import urllib

# MAKE IMAGE LIBRARY

def generateJson(IMG):
    NAMEFILE = os.path.basename(IMG).split('.')[0]
    jsonCheck = Path("json/" + NAMEFILE + '.json')
    if jsonCheck.is_file():
        print('JSon File Exit !!!')
    else:
        subprocess.call(['lumi', 'predict', IMG, '-f', 'json/' + NAMEFILE + '.json'])
    return NAMEFILE;

def cropImg(file):
        imgData = json.loads(open("json/"+file.split(".")[0]+".json").read())
        imgPath = "images/"+file
        img = Image.open(imgPath)
 
        for obj in imgData["objects"]:

            bbox = obj["bbox"]
            label = obj["label"].replace(' ', '_')
            prob = obj["prob"]

            cropped_img = img.crop((bbox[0], bbox[1], bbox[2], bbox[3]))
            cropped_dir = "library/"+label
            probname = str(prob).split('.')[1]
            if len(probname) != 4:
                zero = []
                for u in range(4 - len(probname)):
                    zero += '0' 
                probname = probname + ''.join(zero) 

            firstpartname = probname+"_"+file.split(".")[0]
            print(firstpartname)
            
            if not os.path.exists(cropped_dir):

                os.mkdir(cropped_dir)
                count = 0

            else:
                count = len(glob.glob(cropped_dir+"/"+firstpartname+"/*"))
            
            cropped_path = cropped_dir+"/"+firstpartname+"_"+str("%02d"%count)+".jpg"
            cropped_img.save(cropped_path, "JPEG")




#   GET BLOG CONTENT

def writeJson(dataPath, newJson):
    if not os.path.isfile(dataPath):
        file = open(dataPath, "w")
        file.write(newJson)
        file.close()
        return True
    else:
        oldJson = open(dataPath).read()
        if oldJson != newJson:
           file = open(dataPath, "w")
           file.write(newJson)
           file.close()
           return True
        else:
            return False


def refreshJson(directory, dataPath, newJson):
    if not os.path.isdir(directory):
        os.mkdir(directory)
        return writeJson(dataPath, newJson)
    else:
        return writeJson(dataPath, newJson)

def writeImgs(host, dataPath):
    mails = json.loads(open(dataPath).read())["mails"]
    if not os.path.isdir("images"):
        os.mkdir("images")
    if not os.path.isdir("json"):
        os.mkdir("json")
    if not os.path.isdir("cutImg"):
        os.mkdir("cutImg")
    if not os.path.isdir("library"):
        os.mkdir("library")
    for x in mails:
        if "images" in mails[x]:
            for image in mails[x]["images"]:
                if image["name"]:
                    localImg = "images/"+image["name"]
                    if not os.path.isfile(localImg):
                        distantImg = host+image["path"]
                        urllib.urlretrieve(distantImg, localImg)
                        jsonFile = generateJson(localImg)
                        cropImg(image["name"])
                        drawRects(image["name"])


def getBlogContent():
    host = "http://live.bocto.ch/"
    dataUrl = host+"data/data.json"
    newJson = urllib2.urlopen(dataUrl).read()
    directory = "data" 
    dataPath = directory+"/data.json"
    needsRefresh = refreshJson(directory, dataPath, newJson)

    print(needsRefresh)
    if needsRefresh == True:
        writeImgs(host, dataPath)

    return dataPath


def drawRects(name):
    baseName = name.split('.')[0];
    ext = name.split('.')[1];
    source_img = Image.open('images/' + baseName + "."+ext)
    source_json = json.loads(open("json/" + baseName + '.json').read())

    draw = ImageDraw.Draw(source_img)
    source_img.save('output.jpg', "JPEG")
    for obj in source_json["objects"]:
        rand = random.randint(0, 2)
        print(int(100 * obj['prob']))
        bbox = obj['bbox']
        draw.rectangle(((bbox[0], bbox[1]), (bbox[2], bbox[3])), fill=(int(255 * obj['prob']), 255, 255))
    for obj in source_json["objects"]:
        bbox = obj['bbox']
    source_img.save('cutImg/' + baseName + '.jpg', "JPEG")

getBlogContent()


