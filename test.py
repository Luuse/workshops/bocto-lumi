from PIL import Image
from slugify import slugify
import os
import glob
import json

def cropImgs():

    directory = "json"
        files =  os.listdir("json")

        for file in files: 

            imgData = json.loads(open(directory+"/"+file).read())
                imgName = file.split(".")[0]+".jpg";
                imgPath = "images/"+imgName
                img = Image.open(imgPath)

                for obj in imgData["objects"]:

                    bbox = obj["bbox"]
                        label = obj["label"]
                        prob = obj["prob"]

                        cropped_img = img.crop((bbox[0], bbox[1], bbox[2], bbox[3]))
                        cropped_dir = "library/"+slugify(label)
                        probname = slugify(str(prob)).split("-")[1]
                        firstpartname = probname+"_"+file.split(".")[0]

                        if not os.path.exists(cropped_dir):

                            os.mkdir(cropped_dir)
                                count = 0

                        else:

                            count = len(glob.glob(cropped_dir+"/"+firstpartname+"/*"))


                        cropped_path = cropped_dir+"/"+firstpartname+"_"+str("%02d"%count)+".jpg"
                        cropped_img.save(cropped_path, "JPEG")


cropImgs()
