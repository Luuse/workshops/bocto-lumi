let body = document.getElementsByTagName('BODY')[0]
let wallpaper = document.getElementById('wallpaper')
let wallImg = document.querySelectorAll('#wallpaper .image')

function wallLoad(){

	var i = 1
	l = wallImg.length - 1

	setInterval(function(){

		if(!wallpaper.classList.contains("off")){

			wallImg[i].classList.remove('active')

			if(i < l) { i++ } else { i = 0 } 

				wallImg[i].classList.add('active')	

			var img = wallImg[i].querySelector("img")
			var dataSrc = img.getAttribute('data-src')

			img.setAttribute('src', dataSrc)

		}

	}, 4000)
}

window.addEventListener('DOMContentLoaded', function(){

	var latence = 0;

	if(wallpaper.classList.contains("off")){

		setInterval(function(){

			latence ++

			if(latence > 4){

				wallpaper.classList.remove("off");
				latence = 0

			}

		}, 1000)
	}

	body.addEventListener("mousemove", function(e){

		latence = 0

		if(!wallpaper.classList.contains("off")){

			wallpaper.classList.add("off")

		}
	})

	window.addEventListener("scroll", function(){

		latence = 0

	})

	wallLoad()
})

