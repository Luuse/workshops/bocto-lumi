<?php include_once("core/run.php")  ?> 
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>surfboard bird bottle</title>
	<link rel="stylesheet" href="asset/css/reset.css">
	<link rel="stylesheet" href="asset/css/main.css">
</head>
<body>
	<main>
		<section  id="wallpaper" class="off">
			<?php foreach($data->getWhiteImgs() as $key=>$image): ?>
				<div class="image">
					<img data-src="<?= $image->path ?>" <?= ($key == 1) ? "class='active'" : '' ?>>
					<?php if(isset($image->objects)): ?>
						<?php foreach($image->objects as $object): ?>
							<div class="boxes">
								<div class="label" style="top: <?= $object->y ?>%; left:<?= $object->x ?>%;"><?= $object->label ?></div>
							</div>
						<?php endforeach ?>	
					<?php endif ?>
				</div>
			<?php endforeach ?>	
		</section>
		<section id="index">
			<nav>
				<ul>
					<?php foreach($data->getLibrary() as $category): ?>
						<li><a href="#<?= $category->id ?>"><?= $category->name ?></a></li>
					<?php endforeach ?>
				</ul>
			</nav>
			<div id="images">
				<?php foreach($data->getLibrary() as $category): ?>
					<div class="class" id="<?= $category->id ?>">
						<?php foreach($category->images as $image): ?>
							<figure>
								<img src="<?= $image ?>">
								<figcaption><?= $category->name ?></figcaption>
							</figure>
						<?php endforeach ?>	
					</div>
				<?php endforeach ?>	
			</div>
		</section>
	</main>
</body>
<script src="asset/js/main.js"></script>
</html>
